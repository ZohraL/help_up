<?php

namespace App\tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Process\Process;



class SecurityControllerTest extends WebTestCase
{
    public function setUp(): void
    {
        $process = new Process(['php', 'bin/console', 'do:fi:lo']);
        $process->run();
    }

    public function testRegister()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/register');

        $this->assertResponseIsSuccessful();
        $form = $crawler->selectButton('Submit')->form();
        $form['registration_form[email]'] = 'test@mail.com';
        $form['registration_form[password][first]'] = '1234';
        $form['registration_form[password][second]'] = '1234';
        $form['registration_form[imgPath]'] = 'https://cdn.pixabay.com/photo/2015/01/09/11/11/office-594132_960_720.jpg';
        $client->submit($form);
        $this->assertResponseRedirects('/login');
        $repo = static::$container->get('App\Repository\UserRepository');
        $user = $repo->findOneBy(['email' => 'test@mail.com']);
        $this->assertEquals($user->getEmail(), 'test@mail.com');
    }

    public function testLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $form = $crawler->selectButton('Sign in')->form();
        $form['email'] = 'mail@mail.com';
        $form['password'] = '1234';
        $client->submit($form);
        $crawler = $client->followRedirect();

        $this->assertCount(1, $crawler->filter('.logout'));
    }
}
