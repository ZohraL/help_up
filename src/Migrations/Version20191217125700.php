<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191217125700 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', password VARCHAR(255) NOT NULL, img_path VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE help (id INT AUTO_INCREMENT NOT NULL, creator_id INT DEFAULT NULL, person_max INT NOT NULL, description VARCHAR(255) NOT NULL, date DATETIME NOT NULL, INDEX IDX_8875CAC61220EA6 (creator_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE help_user (help_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_C819E3D2D3F165E7 (help_id), INDEX IDX_C819E3D2A76ED395 (user_id), PRIMARY KEY(help_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE help_technology (help_id INT NOT NULL, technology_id INT NOT NULL, INDEX IDX_1EB69A4FD3F165E7 (help_id), INDEX IDX_1EB69A4F4235D463 (technology_id), PRIMARY KEY(help_id, technology_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ressource (id INT AUTO_INCREMENT NOT NULL, users_id INT DEFAULT NULL, ressource_url VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_939F454467B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ressource_technology (ressource_id INT NOT NULL, technology_id INT NOT NULL, INDEX IDX_3D093A41FC6CD52A (ressource_id), INDEX IDX_3D093A414235D463 (technology_id), PRIMARY KEY(ressource_id, technology_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE technology (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE help ADD CONSTRAINT FK_8875CAC61220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE help_user ADD CONSTRAINT FK_C819E3D2D3F165E7 FOREIGN KEY (help_id) REFERENCES help (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE help_user ADD CONSTRAINT FK_C819E3D2A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE help_technology ADD CONSTRAINT FK_1EB69A4FD3F165E7 FOREIGN KEY (help_id) REFERENCES help (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE help_technology ADD CONSTRAINT FK_1EB69A4F4235D463 FOREIGN KEY (technology_id) REFERENCES technology (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ressource ADD CONSTRAINT FK_939F454467B3B43D FOREIGN KEY (users_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE ressource_technology ADD CONSTRAINT FK_3D093A41FC6CD52A FOREIGN KEY (ressource_id) REFERENCES ressource (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE ressource_technology ADD CONSTRAINT FK_3D093A414235D463 FOREIGN KEY (technology_id) REFERENCES technology (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE help DROP FOREIGN KEY FK_8875CAC61220EA6');
        $this->addSql('ALTER TABLE help_user DROP FOREIGN KEY FK_C819E3D2A76ED395');
        $this->addSql('ALTER TABLE ressource DROP FOREIGN KEY FK_939F454467B3B43D');
        $this->addSql('ALTER TABLE help_user DROP FOREIGN KEY FK_C819E3D2D3F165E7');
        $this->addSql('ALTER TABLE help_technology DROP FOREIGN KEY FK_1EB69A4FD3F165E7');
        $this->addSql('ALTER TABLE ressource_technology DROP FOREIGN KEY FK_3D093A41FC6CD52A');
        $this->addSql('ALTER TABLE help_technology DROP FOREIGN KEY FK_1EB69A4F4235D463');
        $this->addSql('ALTER TABLE ressource_technology DROP FOREIGN KEY FK_3D093A414235D463');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE help');
        $this->addSql('DROP TABLE help_user');
        $this->addSql('DROP TABLE help_technology');
        $this->addSql('DROP TABLE ressource');
        $this->addSql('DROP TABLE ressource_technology');
        $this->addSql('DROP TABLE technology');
    }
}
