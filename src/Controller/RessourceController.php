<?php

namespace App\Controller;

use App\Entity\Ressource;
use App\Form\RessourceType;
use App\Repository\RessourceRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ressource")
 */
class RessourceController extends AbstractController
{
    /**
     * @Route("/", name="ressource_index")
     */
    public function index(RessourceRepository $ressourceRepository)
    {
        return $this->render('ressource/index.html.twig', [
            'ressources' => $ressourceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="ressource_new")
     */
    public function new(Request $request, FileUploader $fileUploader, EntityManagerInterface $entityManager)
    {
        $ressource = new Ressource();
        $form = $this->createForm(RessourceType::class, $ressource);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $ressource->setUsers($this->getUser()); // récupère le user actuellement connecté, indique ds la bdd que le propriétaire de la ressource c'est le user connecté actuellement
            $file = $form->get('file')->getData(); // récupère le fichier passé ds le form par le user
            // assigne le return de la méthode upload contenue ds le service
            $fileName = $fileUploader->upload($file); //ds une variable, j'assigne le retour de la méthode contenue ds le service
            $ressource->setRessourceUrl($fileName); //on attribue filename à la propriété ressourceURL de l'entité ressource 
            //filename = chemin du fichier crée par le service
            //méthode upload gère la création du fichier uploadé, avec un nom unique 
            $entityManager->persist($ressource);
            $entityManager->flush();

            return $this->redirectToRoute('ressource_show');
        }

        return $this->render('ressource/new.html.twig', [
            'ressource' => $ressource,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show", name="ressource_show")
     */
    public function showRessources(RessourceRepository $ressourceRepository)
    {
        return $this->render('ressource/show.html.twig', [
            'ressources' => $ressourceRepository->findAll(),
        ]);
    }
}
