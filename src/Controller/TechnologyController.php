<?php

namespace App\Controller;

use App\Entity\Technology;
use App\Form\TechnologyType;
use App\Repository\TechnologyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/technology")
 */
class TechnologyController extends AbstractController
{
    /**
     * @Route("/", name="technology_index", methods={"GET"})
     */
    public function index(TechnologyRepository $technologyRepository): Response
    {
        return $this->render('technology/index.html.twig', [
            'technologies' => $technologyRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="technology_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $technology = new Technology();
        $form = $this->createForm(TechnologyType::class, $technology);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($technology);
            $entityManager->flush();

            return $this->redirectToRoute('technology_index');
        }

        return $this->render('technology/new.html.twig', [
            'technology' => $technology,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/search", name="technology_search", methods={"GET"})
     */
    public function search(TechnologyRepository $technologyRepository, Request $request)
    {
        $cat = $request->get('selectedCategory'); // sélectionne l'id de la tech
        $selectedTech = null;
        if ($cat != null) {  // input sans valeur
            $selectedTech = $technologyRepository->find($cat); // chope la tech liée à l'id $cat et on l'assigne à $selectedTech
        }
        return $this->render('technology/show.html.twig', [
            'technologies' => $technologyRepository->findAll(),
            'selectedTech' => $selectedTech
        ]);
    }
}
