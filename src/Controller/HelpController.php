<?php

namespace App\Controller;

use App\Entity\Help;
use App\Entity\User;
use App\Form\HelperType;
use App\Form\HelpType;
use App\Repository\HelpRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;



/**
 *@Route("/help")
 */
class HelpController extends AbstractController
{

    /** pour que ttes les ddes s'affichent
     *@Route("/help", name="helps_all")
     */
    public function seeAllHelps(HelpRepository $helpRepository)
    {
        return $this->render('help/index.html.twig', [
            'helps' => $helpRepository->findBy([
                'date' => null
            ]),
        ]);
    }

    /** pour poster une dde d'aide
     *@Route ("/help_add", name="help_add")
     */
    public function addHelp(Request $request, EntityManagerInterface $manager)
    {
        $help = new Help();
        $form = $this->createForm(HelpType::class, $help);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $help->setCreator($this->getUser());
            $manager->persist($help);
            $manager->flush();
        }
        return $this->render('help/new.html.twig', [
            'help' => $help,
            'form' => $form->createView(),
        ]);
    }

    /** pour proposer son aide A AJOUTER : un filtre sur les technologies demandées
     *@Route ("/help_give/{id}", name="help_give")
     */
    public function giveHelp(Request $request, EntityManagerInterface $manager, Help $help)
    {

        $form = $this->createForm(HelperType::class, $help);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $help->setCreator($this->getUser());
            $manager->persist($help);
            $manager->flush();
        }
        return $this->render('help/edit.html.twig', [
            'help' => $help,
            'form' => $form->createView(),
        ]);
    }

    /** pour voir toutes les demandes d'aides avec un rdv fixé
     *@Route ("/show", name="help_show")
     */
    public function showHelps(HelpRepository $helpRepository)
    {
        return $this->render('help/show.html.twig', [
            'helps' => $helpRepository->findAll(),
        ]);
    }

    /** 
     *@Route ("/join/{id}", name="join")
     * grace a l'id contenue ds la route, Doctrine récupère la dde présente en bdd
     * SELECT * FROM help WHERE id = ?
     */
    public function addParticipant(Help $help, EntityManagerInterface $manager)
    {
        $participant = $this->getUser(); // je récupère le user actuellement connecté et je l'assigne à la variable participant
        $help->addParticipant($participant); // vu que j'injecte l'entité Help en argument de ma fonction, j'ai accès aux méthodes
        // de mon entité, comme addParticipant(). Je peux donc ajouter l'user actuellement connecté à une demande d'aide.
        $manager->flush();
        return $this->render('home/index.html.twig', [
            'help' => $participant
        ]);
    }
}
