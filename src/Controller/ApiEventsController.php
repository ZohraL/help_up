<?php

namespace App\Controller;

use App\Entity\Help;
use App\Form\HelperType;
use App\Repository\HelpRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ApiEventsController extends AbstractController
{
    /**
     *@Route("/api/event", name="api_events", methods="GET")
     */
    public function index(HelpRepository $helpRepository)
    {

        return $this->json($helpRepository->findBy([]));
    }

    /**
     * @Route("/api/event/{id}", name="api_add", methods="PATCH")
     * ajouter un rdv à une dde d'aide
     * grace à l'id, trouve la bonne help en bdd et charger dans l'entité help
     */
    public function addEvents(Help $help, Request $request, EntityManagerInterface $manager)
    {

        $form = $this->createForm(HelperType::class, $help, [ // la fonction createform se sert du moule vide helpertype, le lie et le rempli grace à l'entité help
            "csrf_protection" => false // option qui permet de faire des appels en local, une fois désactivé -> sécurité -
        ]);
        $form->submit(
            json_decode( //fonction php pour parse des datas Json en string avec cette fonction $request ...
                $request->getContent(),
                true
            ),
            false
        );

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($help);
            $manager->flush();
            return $this->json($help, 200);
        }

        return $this->json($form->getErrors(true), 400);
    }
}
// application qui renvoit des informations au format json ou autre
