<?php

namespace App\Repository;

use App\Entity\Technology;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Technology|null find($id, $lockMode = null, $lockVersion = null)
 * @method Technology|null findOneBy(array $criteria, array $orderBy = null)
 * @method Technology[]    findAll()
 * @method Technology[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TechnologyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Technology::class);
    }


    /**
     * @return Technology[]
     */
    public function findByKeyword(string $keyword)
    {

        // on met les "%" avant et après le mot clé pour qu'il cherche ce qui contient mm si il y a des choses avant et apres (doit etre fait à l'exterieur de la requette SQL)
        $key = '%' . $keyword . '%';

        $entityManager = $this->getEntityManager();

        // on fait une requette "FROM App\Entity..." et pas directement sur une Table SQL
        // il faut donc mettre un alias (ici "product"), pour qu'il la prenne en compte 
        // il va la considérer comme une "instance de la table" et chercher dedans.

        // là je le fait chercher dans le nom, la categorie et la description, parce que à priori c est les endroits ou un mot clé peut sortir
        $query = $entityManager->createQuery(
            'SELECT technology
            FROM App\Entity\Technology 
            WHERE technology.name LIKE :key'
        )->setParameter('key', $key);

        // returns an array of Product objects
        return $query->getResult();
    }










    // /**
    //  * @return Technology[] Returns an array of Technology objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Technology
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
