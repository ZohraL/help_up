<?php

namespace App\DataFixtures;

use App\Entity\Help;
use App\Entity\Ressource;
use App\Entity\Technology;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;




class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        $user1 = new User();
        $user1->setEmail('mail@mail.com')
            ->setImgPath('https://cdn.pixabay.com/photo/2015/01/09/11/11/office-594132_960_720.jpg')
            ->setPassword('$argon2id$v=19$m=65536,t=4,p=1$3uzOX2V+MGaJFgG3H2/slw$CkuCUVCmOoQtWTGYW93YSQWHA9qi6FssXuUzlw/7N/o')
            ->setRoles(['ROLE USER']);
        $manager->persist($user1);

        $tech1 = new Technology;
        $tech1->setName('Outlook');
        $manager->persist($tech1);
        $tech2 = new Technology;
        $tech2->setName('Excel');
        $manager->persist($tech2);
        $tech3 = new Technology;
        $tech3->setName('Logiciel de gestion de congés');
        $manager->persist($tech3);
        $tech4 = new Technology;
        $tech4->setName('CRM');
        $manager->persist($tech4);
        $tech5 = new Technology;
        $tech5->setName('Access');
        $manager->persist($tech5);
        $tech6 = new Technology;
        $tech6->setName('Adobe/PDF');
        $manager->persist($tech6);
        $tech7 = new Technology;
        $tech7->setName('Logiciel de gestion du temps de travail');
        $manager->persist($tech7);

        $help1 = new Help();
        $help1->setCreator($user1)
            ->setDate($faker->dateTimeBetween('now', '+10 days'))
            ->setDescription('Problème pour mettre un message automatique')
            ->setPersonMax(10)
            ->addTechnology($tech1);
        $manager->persist($help1);

        $ressource1 = new Ressource();
        $ressource1->setRessourceUrl('fixture1.pdf')
            ->setType('document')
            ->setUsers($user1);
        $manager->persist($ressource1);


        for ($i = 0; $i < 3; $i++) {
            $user = new User();
            if ($i == 0) {
                $user->setEmail('test@test.com');
            } else {

                $user->setEmail($faker->email);
            }
            $user->setImgPath($faker->imageUrl());
            $user->setPassword('$argon2id$v=19$m=65536,t=4,p=1$3uzOX2V+MGaJFgG3H2/slw$CkuCUVCmOoQtWTGYW93YSQWHA9qi6FssXuUzlw/7N/o');
            $user->setRoles(['ROLE_USER']);
            $manager->persist($user);
            for ($y = 0; $y < 3; $y++) {
                $help = new Help();
                $help->setCreator($user);
                $help->setDate($faker->dateTimeBetween('now', '+10 days'));
                $help->setDescription($faker->text($maxNbChars = 200));
                $help->setPersonMax(10)
                    ->addTechnology($tech1);
                $manager->persist($help);
            }
        }

        $manager->flush();
    }
}
