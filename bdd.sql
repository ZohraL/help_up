CREATE DATABASE help_up; -- do:da:cre
USE help_up; 

CREATE help ( --ma:mi/do:mi:mi
id INT PRIMARY KEY AUTO_INCREMENT NOT NULL, --identifie de manière unique un élément d'une table, permet de différencier deux helps qui auraient le mm name grace à l'auto incrément
personMax INT,
description VARCHAR (64),
date DATETIME

);

--select join : récupérer tous les users et les aides qu'ils ont crée : 
-- SELECT * FROM user LEFT JOIN help ON user.id = help.creator; 
--update : UPDATE help set description = 'blablabla' WHERE id = 2; 
--delete : DELETE FROM user WHERE id = 3; 
--insert into :  INSERT INTO user (email, pwd, imgPath, roles) VALUES ('valeur 1', 'valeur 2', 'valeur 3', 'valeur 4');

-- migration fichier qui contient toutes les requetes SQL pour que la bdd correspondent aux entités.